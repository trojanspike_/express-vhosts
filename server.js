//
// Module dependencies
//

var express = require('express');
var vhost = require('vhost');
var app = express();


//
// vhosts
//

app
  .use(vhost('app1.io', require('./app1/app.js')))
  .use(vhost('app2.io', require('./app2/app.js')))
  .use(vhost('app3.io', require('./app3/app.js')))
  .listen(8080);
